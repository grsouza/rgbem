extern crate clap;
use clap::{App, Arg};

mod cpu;
mod memory;

use cpu::CPU;
use memory::Memory;

struct Simulator {
    cpu: CPU,
}

impl Simulator {
    fn new(rom_path: &str) -> Simulator {
        let memory = Memory::load(rom_path);
        let cpu = CPU::new(memory);
        Simulator { cpu: cpu }
    }

    fn run(&self) {}
}

fn main() {
    let matches = App::new("Rust Game Boy Emulator")
        .version("0.0.1")
        .author("Guilherme Souza")
        .about("A Game Boy Emulator")
        .arg(
            Arg::with_name("rom")
                .short("r")
                .long("rom")
                .help("ROM file to load")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let rom_path = matches.value_of("rom").unwrap();
    let simulator = Simulator::new(rom_path);

    simulator.run();
}
