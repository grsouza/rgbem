use memory::Memory;

pub struct CPU {
  memory: Memory,
}

impl CPU {
  pub fn new(memory: Memory) -> CPU {
    CPU { memory }
  }
}

enum InstructionKind {
  NoOperator,
  Immediate16Bits,
  Immediate8Bits,
  Address16Bits,
  Address8Bits,
}

#[allow(dead_code)]
const INSTRUCTIONS: [Instruction; 32] = [
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "NOP",
  },
  Instruction {
    kind: InstructionKind::Immediate16Bits,
    assembly: "LD BC, 0x{:4X}",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "LD (BC), A",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "INC BC",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "INC B",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "DEC B",
  },
  Instruction {
    kind: InstructionKind::Immediate8Bits,
    assembly: "LD B, 0x{:2X}",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "RLCA",
  },
  Instruction {
    kind: InstructionKind::Address16Bits,
    assembly: "LD (0x{:4X}), SP",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "ADD HL, BC",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "LD A, (BC)",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "DEC BC",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "INC C",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "DEC C",
  },
  Instruction {
    kind: InstructionKind::Immediate8Bits,
    assembly: "LD C, 0x{:2X}",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "RRCA",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "STOP 0",
  },
  Instruction {
    kind: InstructionKind::Immediate16Bits,
    assembly: "LD DE, 0x{:4X}",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "LD (DE), A",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "INC DE",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "INC D",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "DEC D",
  },
  Instruction {
    kind: InstructionKind::Immediate8Bits,
    assembly: "LD D, 0x{:2X}",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "RLA",
  },
  Instruction {
    kind: InstructionKind::Address8Bits,
    assembly: "JR 0x{:2X}",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "ADD HL, DE",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "LD A, (DE)",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "DEC DE",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "INC E",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "DEC E",
  },
  Instruction {
    kind: InstructionKind::Immediate8Bits,
    assembly: "LD E, 0x{:2X}",
  },
  Instruction {
    kind: InstructionKind::NoOperator,
    assembly: "RRA",
  },
];

struct Instruction {
  kind: InstructionKind,
  assembly: &'static str,
}
