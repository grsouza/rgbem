use std::fmt;
use std::fs::File;
use std::io::prelude::*;

pub struct Memory {
  rom: Vec<u8>,
}

impl Memory {
  pub fn load(rom_path: &str) -> Memory {
    let mut file = File::open(rom_path).expect("File not found");
    let mut bytes: Vec<u8> = vec![];
    file
      .read_to_end(&mut bytes)
      .expect("Something went wrong while reading file");

    Memory { rom: bytes }
  }

  pub fn read(&mut self, location: usize) {}
  pub fn write(&mut self, value: usize, location: usize) {}
}
