# RGBem
RGBem stands for **R**ust **G**ame **B**oy **Em**ulator.

- [Opcodes](http://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html)
- [GameBoy CPU Manual](http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf)
